class Conjugate {

    constructor(verb, tense) {
        this.verb = verb;
        this.tense = tense;
    }

    conjugate() {
        this[this.tense.tense.toLowerCase()]();
    }

    present() {

    }

    past() {

    }
    te() {

    }
    volitional() {

    }
    potential() {

    }
    passive() {

    }
    causative() {

    }
    imperative() {

    }
    conditional() {

    }
    conditionalTara() {

    }

}