const godan = require('./ConjugateGodan')

const hiragana = "あいうえおかきくけこがぎぐげごさしすせそざじずぜぞたちつてとだぢづでどなにぬねのはひふへほばびぶべぼぱぴぷぺぽまみむめもや　ゆ　よらりるれろわ　　　をん　　　　"
const tense = ["Present","Past","Te","Volitional","Potential","Passive","Causative","Imperative","Conditional","ConditionalTara"]
const tenseForm = ["Positive Plain","Positive PoliTe","Negative Plain","Negative PoliTe"]
const group = ["Godan","Ichidan","Irrigular"]

const verbList = await loadVerbList();

function randomFromEnum(enumObject) {
    const values = Object.values(enumObject);
    const randomIndex = Math.floor(Math.random() * values.length);
    return values[randomIndex];
}


function conjugate(verb, tense) {
    console.log(verb)
    console.log(tense)
    return window[verb.group.toLowerCase()](verb, tense).conjugate()
    switch(verb.group)
    {
        case "Godan":
            return new ConjugateGodan(verb, tense).conjugate()
        case "Ichidan":
            return ConjugateIchidan(verb, tense)
        case "Irrigular":
            return ConjugateIrregular(verb, tense)
    }
}
//TODO: BetTer Error Handeling
//TODO: return full verb info
function conjugateGodan(verb, Tense) {
    const verbWithoutEnding = verb.kanji.slice(0,-1)
    let ending = verb.kanji.slice(-1)
    const hiraganaPos = GetHiraganaRowCol(ending)

    switch(Tense.tense)
    {
        case "Present":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verb.kanji
                case "Positive PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 1] + "ます"
                case "Negative Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 2] + "ない"
                case "Negative PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 1] + "ません"
            }
        case "Past":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 1] + "た"
                case "Positive PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 1] + "ました"
                case "Negative Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 2] + "なかった"
                case "PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 1] + "ませんでした"
            }
        case "Te":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Positive PoliTe":
                    return verbWithoutEnding + TeFormGodan(ending)
                case "Negative Plain":
                case "Negative PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 2] + "なくて"
            }
        case "Volitional":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Negative Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c + 2] + "う"
                case "Positive PoliTe":
                case "Negative PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 1] + "ましょう"
            }
        case "Potential":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c + 1] + "る"
                case "Positive PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c + 1] + "ます"
                case "Negative Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c + 1] + "ない"
                case "PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c + 1] + "ません"
            }
        case "Passive":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "れる"
                case "Positive PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "れます"
                case "Negative Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "れない"
                case "PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "れません"
            }
        case "Causative":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "せる"
                case "Positive PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "せます"
                case "Negative Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "せない"
                case "PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r == 0 ? 14 : hiraganaPos.r, hiraganaPos.c - 2] + "せません"
            }
        case "Imperative":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c + 1]
                case "Positive PoliTe":
                    return verbWithoutEnding + TeFormGodan(ending) + "ください"
                case "Negative Plain":
                    return verb.kanji + "な"
                case "PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 2] + "ないでください"
            }
        case "Conditional":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Positive PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c + 1] + "ば"
                case "Negative Plain":
                case "Negative PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 1] + "なければ"
            }
        case "Conditional Tara":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Positive PoliTe":
                    var Te = TeFormGodan(ending)
                    ending = Te.slice(-1) == "て" ? "たら" : "だら"

                    return verbWithoutEnding + Te.slice(0,-1) + ending
                case "Negative Plain":
                case "Negative PoliTe":
                    return verbWithoutEnding + hiragana[hiraganaPos.r, hiraganaPos.c - 2] + "なかったら"
            }
    }

    return "This Tense don’t exist"
}
//TODO: BetTer Error Handeling
//TODO: return full verb info
function ConjugateIchidan(verb, Tense) {
    const verbWithoutEnding = verb.kanji.slice(0,-1)

    switch(Tense.tense)
    {
        case "Present":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verb.kanji
                case "Positive PoliTe":
                    return verbWithoutEnding + "ます"
                case "Negative Plain":
                    return verbWithoutEnding + "ない"
                case "Negative PoliTe":
                    return verbWithoutEnding + "ません"
            }
        case "Past":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + "た"
                case "Positive PoliTe":
                    return verbWithoutEnding + "ました"
                case "Negative Plain":
                    return verbWithoutEnding + "なかった"
                case "PoliTe":
                    return verbWithoutEnding + "ませんでした"
            }
        case "Te":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Positive PoliTe":
                    return verbWithoutEnding + "て"
                case "Negative Plain":
                case "Negative PoliTe":
                    return verbWithoutEnding + "なくて"
            }
        case "Volitional":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Negative Plain":
                    return verbWithoutEnding + "よう"
                case "Positive PoliTe":
                case "Negative PoliTe":
                    return verbWithoutEnding + "ましょう"
            }
        case "Potential":
        case "Passive":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + "られる"
                case "Positive PoliTe":
                    return verbWithoutEnding + "られます"
                case "Negative Plain":
                    return verbWithoutEnding + "られない"
                case "PoliTe":
                    return verbWithoutEnding + "られません"
            }
        case "Causative":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + "させる"
                case "Positive PoliTe":
                    return verbWithoutEnding + "させます"
                case "Negative Plain":
                    return verbWithoutEnding + "させない"
                case "PoliTe":
                    return verbWithoutEnding + "させません"
            }
        case "Imperative":
            switch(Tense.form)
            {
                case "Positive Plain":
                    return verbWithoutEnding + "よ/ろ"
                case "Positive PoliTe":
                    return verbWithoutEnding + "ください"
                case "Negative Plain":
                    return verb.kanji + "な"
                case "PoliTe":
                    return verbWithoutEnding + "ないでください"
            }
        case "Conditional":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Positive PoliTe":
                    return verbWithoutEnding + "れば"
                case "Negative Plain":
                case "Negative PoliTe":
                    return verbWithoutEnding + "なければ"
            }
        case "Conditional Tara":
            switch(Tense.form)
            {
                case "Positive Plain":
                case "Positive PoliTe":
                    return verbWithoutEnding + "たら"
                case "Negative Plain":
                case "Negative PoliTe":
                    return verbWithoutEnding + "なかったら"
            }
    }

    return "This Tense don’t exist"
}
function ConjugateIrregular(verb, Tense) {

}

function TeFormGodan(ending) {
    if ("うつる".includes(ending))
        ending = "って"
    if ("むぶぬ".includes(ending))
        ending = "んで"
    if (ending == "く")
        ending = "いて";
    if (ending == "ぐ")
        ending = "いで";
    if (ending == "す")
        ending = "して";
    return ending;
}

//TODO: BetTer Error Handeling
function GetHiraganaRowCol(hiragana) {
    for(let i=0; i<hiragana.length; i++)
        if(hiragana.charAt(i) == hiragana)
            return {r:i/4, c:i%4}

    return "Not Hiragana"
}

async function loadVerbList() {
    console.log("Loading Verb List")
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", "data.csv", true);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4 && (rawFile.status === 200 || rawFile.status === 0)) {
            // FileReader Object
            var reader = new FileReader();
            var blob = new Blob([rawFile.responseText], { type: 'Text/plain' });

            // Read file as string
            reader.readAsText(blob);
            // Load event
            reader.onload = function () {
                reader.result.split('\n').forEach(line => {
                    const verb = line.split(',');
                    verbList.push({kanji: verb[0], kana: verb[1], group:verb[2]})
                });
            };
        }
    }
    rawFile.send(null)
}